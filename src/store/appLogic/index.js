import PropTypes from 'prop-types/prop-types';
import { kea } from 'kea';
import { cutOffBase, preventCors } from '../../utils';

export default kea({
    path: () => ['scenes', 'info', 'app'],

    actions: () => ({
        changeUserImg: (data) => data,
        changeResultImg: (data) => data,
        changeIsLoading: (bool) => bool
    }),

    reducers: ({ actions }) => ({
        isLoading: [
            false,
            PropTypes.bool,
            {
                [actions.changeIsLoading]: (state, payload) => payload
            }
        ],
        userImg: [
            null,
            PropTypes.string,
            {
                [actions.changeUserImg]: (state, payload) => payload,
                [actions.changeResultImg]: (state, payload) => null
            }
        ],
        resultImg: [
            null,
            PropTypes.string,
            {
                [actions.changeUserImg]: (state, payload) => null,
                [actions.changeResultImg]: (state, payload) => payload
            }
        ]
    }),

    thunks: ({ actions }) => ({
        getResultImg: async (img, back = `https://photohack-june-2019.herokuapp.com/process`) => {
            actions.changeIsLoading(true);
            fetch(preventCors(back, false), {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    img: cutOffBase(img)
                })
            })
                .then((response) => response.json())
                .then((data) => {
                    actions.changeIsLoading(false);
                    return actions.changeResultImg(data.img);
                })
                .catch((error) => {
                    console.error(error);
                    actions.changeIsLoading(false);
                });
        }
    })
});
