/** @jsx jsx */
import { jsx } from '@emotion/core';

import appLogic from '../../store/appLogic';

import Button from '@material-ui/core/Button';

import { Dropzone, Loader, StyledPageContainer, StyledButtonContainer } from './units';

import { connect } from 'kea';
const connectedLogic = connect({
    actions: [appLogic, ['changeUserImg', 'getResultImg']],
    props: [appLogic, ['userImg', 'resultImg', 'isLoading']]
});

function App(props) {
    const { userImg, resultImg, isLoading, actions, back } = props;
    const { changeUserImg, getResultImg } = actions;
    return (
        <StyledPageContainer>
            <Loader active={isLoading} />
            <Dropzone img={resultImg || userImg} onChange={(img) => changeUserImg(img)} />
            <StyledButtonContainer>
                <Button
                    size="large"
                    color="secondary"
                    variant="contained"
                    disabled={!userImg || isLoading}
                    onClick={() => getResultImg(userImg, back)}>
                    Обработать фото
                </Button>
            </StyledButtonContainer>
        </StyledPageContainer>
    );
}

export default connectedLogic(App);
