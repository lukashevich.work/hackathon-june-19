/** @jsx jsx */
import { useState } from 'react';
import { jsx, css } from '@emotion/core';
import styled from '@emotion/styled';
import uuid from 'uuid/v1';

import { colors } from '../../../utils';

import { DropzoneArea } from 'material-ui-dropzone';

const DropzoneWrapper = styled.div`
    display: inline-block;
    width: auto;
    ${({ img }) =>
        !img &&
        css`
            width: 100%;
            height: 50vh;
        `};
    max-width: 100%;
    max-height: 100%;
    border: 3px dashed ${colors.primary};
    border-radius: 15px;
    overflow: hidden;
    position: relative;
    .dzone-area {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 100%;
        padding: 15px;
        background: no-repeat center, transparent;
        ${({ img }) =>
            img &&
            css`
                opacity: 0;
            `};
        background-size: cover;
        border: none;
        ${({ img }) =>
            img &&
            css`
                position: absolute !important;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
            `}
    }
    .dzone-p {
        font-size: 18px;
        transition: 0.3s opacity;
    }
    svg {
        color: currentColor;
        transition: 0.3s opacity;
    }
    &:not(:hover) .dzone-p,
    &:not(:hover) svg {
        ${({ img }) =>
            img &&
            css`
                opacity: 0;
            `};
    }
`;

const StyledImg = styled.img`
    display: block;
    margin: 0 auto;
    max-width: 100%;
    max-height: 70vh;
`;

function handleImageChange(file, cb) {
    const reader = new FileReader();
    reader.onloadend = () => {
        cb(reader.result);
    };
    reader.readAsDataURL(file);
}

export default (props) => {
    const { img, onChange } = props;
    const [dzKey, setDzKey] = useState(uuid());

    return (
        <DropzoneWrapper img={img}>
            {img ? <StyledImg src={img} alt="" /> : null}
            <DropzoneArea
                key={dzKey}
                onChange={(files) => {
                    if (files[0]) {
                        files[0] && handleImageChange(files[0], onChange);
                        setDzKey(uuid());
                    }
                }}
                dropzoneText="Drag and drop your photo file here or click"
                showAlerts={false}
                filesLimit={1}
                dropZoneClass="dzone-area"
                acceptedFiles={['image/*']}
                dropzoneParagraphClass="dzone-p"
            />
        </DropzoneWrapper>
    );
};
