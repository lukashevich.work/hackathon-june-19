import styled from '@emotion/styled';

import Container from '@material-ui/core/Container';

import Loader from './Loader';
import Dropzone from './Dropzone';

const StyledPageContainer = styled(Container)`
    height: 100%;
    padding: 16px;
`;

const StyledButtonContainer = styled.div`
    display: flex;
    justify-content: center;
    padding-top: 20px;
    .Mui-disabled[disabled] {
        color: rgba(255, 255, 255, 0.3);
    }
`;

export { StyledPageContainer, StyledButtonContainer, Loader, Dropzone };
