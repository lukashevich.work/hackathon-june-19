/** @jsx jsx */
import { jsx } from '@emotion/core';
import styled from '@emotion/styled';

import Grid from '@material-ui/core/Grid';

import Slider from '@material-ui/lab/Slider';

const StyledSliderCaption = styled.span`
    display: inline-block;
    min-width: 80px;
    margin-right: 16px;
    white-space: nowrap;
`;

const StyledSlider = styled(Slider)`
    &.MuiSlider-root {
        color: #ff719d;
    }
`;

export default (props) => {
    const { children, steps, value, onChange } = props;
    return (
        <Grid container={true} wrap={'nowrap'} alignItems={'flex-start'}>
            <StyledSliderCaption>{children}</StyledSliderCaption>
            <StyledSlider
                marks={true}
                max={steps}
                value={value}
                onChange={(event, newValue) => {
                    if (onChange && newValue !== value) onChange(newValue);
                }}
            />
        </Grid>
    );
};
