/** @jsx jsx */
import { jsx } from '@emotion/core';
import styled from '@emotion/styled';

import CircularProgress from '@material-ui/core/CircularProgress';

const StyledContainer = styled.div`
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: rgba(0, 0, 0, 0.3);
    z-index: 999;
    svg {
        transform: scale(2);
    }
`;

export default (props) => {
    const { active } = props;
    return (
        active && (
            <StyledContainer>
                <CircularProgress color="secondary" />
            </StyledContainer>
        )
    );
};
