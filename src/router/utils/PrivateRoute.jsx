import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, publicRoute, title, back, ...rest }) => (
    <Route
        {...rest}
        render={(props) =>
            publicRoute ? (
                <Component back={back} {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: '/login',
                        state: { from: props.location }
                    }}
                />
            )
        }
    />
);

export default PrivateRoute;
