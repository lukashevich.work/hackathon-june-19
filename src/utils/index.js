function preventCors(url, isActive = true) {
    const proxy = 'https://cors-anywhere.herokuapp.com/';
    return isActive ? proxy + url : url;
}

function cutOffBase(url) {
    const base = ';base64,';
    return url.split(base)[1];
}

const colors = {
    primary: '#ff719d'
};

export { preventCors, colors, cutOffBase };
